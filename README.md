# Workery Log

Logs to Workery system.

## Environment variables

The environment variables below, when present, activate the following functionalities:

- WORKERY_LOG:

    If true, show messages in console.

- WORKERY_LOG_PATH:

    If setted, writes to files in the defined folder.

- WORKERY_COUCHDB_URI:

    Writes logs to CouchDB

## Message types

* success
* info
* debug
* warning
* error

## How to use

### Initialize

```python
from workery_log import Logger

logger = Logger(app='My portentous app')
# If you want to identify another point,
# such as calling a module or class or method,
# just call it again:
logger(unit='My wondrous module')
```

### Use

```python
# You can send a simple success message...:
logger.success('Everything worked!')
>>> [SUCCESS] 2024-03-22 18:36 -> message="Everything worked!" app="My portentous app" unit="My wondrous module"
# ...or a message with supplementary arguments:
logger.error('Something wrong is not right', what='anything')
>>> [ERROR] 2024-03-22 18:36 -> message="Something wrong is not right" app="My portentous app" unit="My wondrous module" what="anything"
```
