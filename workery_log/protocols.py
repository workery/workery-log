from typing import (
    NoReturn,
    Self
)


class Protocol:

    async def error(
        Self
    ) -> NoReturn:
        ...

    async def warning(
        Self
    ) -> NoReturn:
        ...

    async def info(
        Self
    ) -> NoReturn:
        ...

    async def debug(
        Self
    ) -> NoReturn:
        ...

    async def success(
        Self
    ) -> NoReturn:
        ...

    async def security(
        Self
    ) -> NoReturn:
        ...
