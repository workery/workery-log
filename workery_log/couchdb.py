from datetime import datetime
from httpx import Client
from pathlib import Path
from typing import (
    LiteralString,
    NoReturn,
    Optional,
    Self,
    Union
)
from workery_log.constants import COUCHDB_URI
from workery_log.protocols import (
    Protocol
)


class CouchDB(Protocol):

    __client = Client(
        base_url=COUCHDB_URI
    )

    def __write(
        self: Self,
        db: str,
        data: dict
    ) -> NoReturn:
        data.update(
            {
                'date': datetime.utcnow(
                    ).isoformat()
            }
        )
        self.__client.put(
            url=db
        )
        self.__client.post(
            url=db,
            json=data
        )

    async def security(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        data = {
            'message': message
        }
        [
            data.update(
                {
                    kwarg: kwargs[kwarg]
                }
            ) for kwarg in kwargs
        ]
        self.__write(
            db='security',
            data=data
        )

    async def error(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        data = {
            'message': message
        }
        [
            data.update(
                {
                    kwarg: kwargs[kwarg]
                }
            ) for kwarg in kwargs
        ]
        self.__write(
            db='error',
            data=data
        )

    async def warning(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        data = {
            'message': message
        }
        [
            data.update(
                {
                    kwarg: kwargs[kwarg]
                }
            ) for kwarg in kwargs
        ]
        self.__write(
            db='warning',
            data=data
        )

    async def info(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        data = {
            'message': message
        }
        [
            data.update(
                {
                    kwarg: kwargs[kwarg]
                }
            ) for kwarg in kwargs
        ]
        self.__write(
            db='info',
            data=data
        )

    async def debug(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        data = {
            'message': message
        }
        [
            data.update(
                {
                    kwarg: kwargs[kwarg]
                }
            ) for kwarg in kwargs
        ]
        self.__write(
            db='debug',
            data=data
        )

    async def success(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        data = {
            'message': message
        }
        [
            data.update(
                {
                    kwarg: kwargs[kwarg]
                }
            ) for kwarg in kwargs
        ]
        self.__write(
            db='success',
            data=data
        )
