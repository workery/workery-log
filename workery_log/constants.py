from os import getenv


TEXT_LOG=getenv(
    'WORKERY_LOG'
) or None
LOG_PATH=getenv(
    'WORKERY_LOG_PATH'
) or None
COUCHDB_URI=getenv(
    'WORKERY_COUCHDB_URI'
) or None
