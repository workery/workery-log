if __name__ == '__main__':
    from sys import argv
    from workery_log.loggers import Logger


    if len(argv) > 2:
        text = ''
        for argument in range(len(argv) - 2):
            text += f'{argv[argument + 2]} '
        log = Logger()
        match argv[1]:
            case 'error':
                log.error(
                    text
                )
            case 'warning':
                log.warning(
                    text
                )
            case 'info':
                log.info(
                    text
                )
            case 'debug':
                log.debug(
                    text
                )
            case 'success':
                log.success(
                    text
                )
