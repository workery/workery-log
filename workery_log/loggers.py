from pathlib import Path
from typing import (
    LiteralString,
    NoReturn,
    Optional,
    Self,
    Union
)
from workery_log.constants import (
    COUCHDB_URI,
    LOG_PATH,
    TEXT_LOG
)
from workery_log.couchdb import (
    CouchDB
)
from workery_log.files import (
    File
)
from workery_log.messages import (
    Message
)
from workery_log.protocols import (
    Protocol
)


class Logger(Protocol):

    __plugins = []

    def __init__(
        self: Self,
        app: Optional[str] = None
    ) -> NoReturn:
        self.app = app
        self.unit = None

        if TEXT_LOG:
            self.__plugins.append(
                Message()
            )
        if LOG_PATH:
            self.__plugins.append(
                File()
            )
        if COUCHDB_URI:
            self.__plugins.append(
                CouchDB()
            )

    def __call__(
        self: Self,
        unit: Optional[str] = None
    ) -> NoReturn:
        self.unit = unit

    async def security(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> str:
        [
            await plugin.security(
                message,
                app=self.app,
                unit=self.unit,
                *args, **kwargs
            ) for plugin in self.__plugins
        ]

    async def error(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> str:
        [
            await plugin.error(
                message,
                app=self.app,
                unit=self.unit,
                *args, **kwargs
            ) for plugin in self.__plugins
        ]

    async def warning(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> str:
        [
            await plugin.warning(
                message,
                app=self.app,
                unit=self.unit,
                *args, **kwargs
            ) for plugin in self.__plugins
        ]

    async def info(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> str:
        [
            await plugin.info(
                message,
                app=self.app,
                unit=self.unit,
                *args, **kwargs
            ) for plugin in self.__plugins
        ]

    async def debug(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> str:
        [
            await plugin.debug(
                message,
                app=self.app,
                unit=self.unit,
                *args, **kwargs
            ) for plugin in self.__plugins
        ]

    async def success(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> str:
        [
            await plugin.success(
                message,
                app=self.app,
                unit=self.unit,
                *args, **kwargs
            ) for plugin in self.__plugins
        ]
