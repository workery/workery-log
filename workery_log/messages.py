from datetime import datetime
from pathlib import Path
from typing import (
    LiteralString,
    NoReturn,
    Optional,
    Self,
    Union
)
from workery_log.protocols import (
    Protocol
)


class Message(Protocol):
    __reset = '\033[m'

    def __show(
        self: Self,
        color: str,
        reason: str,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        output = f'message="{ message }"'
        for arg in kwargs:
            output += f' { arg }="{ kwargs[arg] }"'
        text = '[' + reason.upper(
        ) + '] ' + datetime.utcnow(
        ).strftime(
            '%Y-%m-%d %H:%M'
        ) + ' -> ' + output
        print(
            color +\
            text +\
            self.__reset
        )

    async def security(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> str:
        self.__show(
            color='\033[1;31m',
            message=message,
            reason='error',
            *args, **kwargs
        )

    async def error(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> str:
        self.__show(
            color='\033[1;31m',
            message=message,
            reason='error',
            *args, **kwargs
        )

    async def warning(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> str:
        self.__show(
            color='\033[1;33m',
            message=message,
            reason='warning',
            *args, **kwargs
        )

    async def info(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> str:
        self.__show(
            color='\033[1m',
            message=message,
            reason='info',
            *args, **kwargs
        )

    async def debug(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> str:
        self.__show(
            color='\033[m',
            message=message,
            reason='debug',
            *args, **kwargs
        )

    async def success(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> str:
        self.__show(
            color='\033[1;32m',
            message=message,
            reason='success',
            *args, **kwargs
        )
