from datetime import datetime
from pathlib import Path
from typing import (
    LiteralString,
    NoReturn,
    Optional,
    Self,
    Union
)
from workery_log.constants import (
    LOG_PATH
)
from workery_log.protocols import (
    Protocol
)


class File(Protocol):
    def __init__(
        self: Self
    ) -> NoReturn:
        self.folder = Path(
            LOG_PATH
        ).absolute()
        self.folder.mkdir(
            parents=True,
            exist_ok=True
        )

    def __write(
        self: Self,
        reason: str,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        output = f'message="{ message }"'
        for arg in kwargs:
            output += f' { arg }="{ kwargs[arg] }"'
        formatted_message = datetime.utcnow(
        ).strftime(
            '%Y-%m-%d %H:%M'
        ) + ' -> ' + output + '\n'
        self.path = Path(
            self.folder / reason
        )
        with open(
            self.path,
            mode='a',
            encoding='utf8',
            newline='\n'
        ) as file:
            file.write(
                formatted_message
            )

    async def security(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        self.__write(
            reason='security',
            message=message,
            *args, **kwargs
        )

    async def error(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        self.__write(
            reason='error',
            message=message,
            *args, **kwargs
        )

    async def warning(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        self.__write(
            reason='warning',
            message=message,
            *args, **kwargs
        )

    async def info(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        self.__write(
            reason='info',
            message=message,
            *args, **kwargs
        )

    async def debug(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        self.__write(
            reason='debug',
            message=message,
            *args, **kwargs
        )

    async def success(
        self: Self,
        message: str,
        *args, **kwargs
    ) -> NoReturn:
        self.__write(
            reason='success',
            message=message,
            *args, **kwargs
        )
