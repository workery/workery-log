include .env

couch:
	sudo podman run \
		--name couchdb-workery \
		--publish 5984:5984 \
		--env COUCHDB_USER=${WORKERY_NAME} \
		--env COUCHDB_PASSWORD=${WORKERY_SECRET} \
		--detach --rm --replace \
		couchdb:latest
